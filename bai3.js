/** 
 * Bước 1: nhập input từ user về số thứ nhất, số thứ hai, số thứ ba.
 * Bước 2: nhập các điều kiện số chia lấy dư cho 2 có dư khác 0 thì sẽ lẻ, còn nếu không dư là số chẵn. 
 * Bước 3: in kết quả
*/


function sapXepTheoThuTu() {
    const sothunhat = document.getElementById("so-thu-nhat").value * 1;
    const sothuhai = document.getElementById("so-thu-hai").value * 1;
    const sothuba = document.getElementById("so-thu-ba").value * 1;
    
    if (sothunhat % 2 == 0 && sothuhai % 2 == 0 && sothuba % 2 == 0) {
        document.getElementById("result").innerHTML = "có 3 số chẵn"
    } else if (sothunhat % 2 != 0 && sothuhai % 2 == 0 && sothuba % 2 == 0) {
        document.getElementById("result").innerHTML = "có 2 số chẵn 1 số lẻ"
    } else if (sothunhat % 2 != 0 && sothuhai % 2 != 0 && sothuba % 2 == 0) {
        document.getElementById("result").innerHTML = "có 1 số chẵn 2 số lẻ"
    } else if (sothunhat % 2 != 0 && sothuhai % 2 != 0 && sothuba % 2 != 0) {
        document.getElementById("result").innerHTML = "có 3 số lẻ"
    }  else if (sothunhat % 2 == 0 && sothuhai % 2 != 0 && sothuba % 2 == 0) {
        document.getElementById("result").innerHTML = "có 2 số chẵn 1 số lẻ"
    } else if (sothunhat % 2 == 0 && sothuhai % 2 != 0 && sothuba % 2 != 0) {
        document.getElementById("result").innerHTML = "có 1 số chẵn 2 số lẻ"
    }  else if (sothunhat % 2 == 0 && sothuhai % 2 == 0 && sothuba % 2 != 0) {
        document.getElementById("result").innerHTML = "có 2 số chẵn 1 số lẻ"
    }  else if (sothunhat % 2 != 0 && sothuhai % 2 == 0 && sothuba % 2 != 0) {
        document.getElementById("result").innerHTML = "có 1 số chẵn 2 số lẻ"
    }  
}   
