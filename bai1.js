/** 
 * Bước 1: nhập lấy nguyên tố số thứ nhất, thứ hai, thứ ba bằng cách getelementid từ user nhập;
 * Bước 2: so sánh từng giá trị và sắp xếp lại theo thứ tự tăng dần
 * Bước 3: in kết quả
*/



function sapXepTheoThuTu() {
    const sothunhat = document.getElementById("so-thu-nhat").value * 1;
    const sothuhai = document.getElementById("so-thu-hai").value * 1;
    const sothuba = document.getElementById("so-thu-ba").value * 1;
    
    if (sothunhat >= sothuhai && sothuhai >= sothuba) {
        document.getElementById("result").innerHTML = sothuba + "" + sothuhai + "" + sothunhat;
    } else if (sothunhat <= sothuhai && sothuhai >= sothuba && sothuba >= sothunhat) {
        document.getElementById("result").innerHTML = sothunhat + "" + sothuba + "" + sothuhai;
    } else if (sothuba >= sothuhai && sothuhai >= sothunhat) {
        document.getElementById("result").innerHTML = sothunhat + "" + sothuhai + "" + sothuba;
    } else if (sothunhat <= sothuhai && sothuhai >= sothuba && sothuba <= sothunhat) {
        document.getElementById("result").innerHTML = sothuba + "" + sothunhat + "" + sothuhai;
    } else if (sothunhat >= sothuhai && sothuhai <= sothuba && sothuba <= sothunhat) {
        document.getElementById("result").innerHTML = sothuhai + "" + sothuba + "" + sothunhat;
    } else if (sothunhat >= sothuhai && sothuhai <= sothuba && sothuba >= sothunhat) {
        document.getElementById("result").innerHTML = sothuhai + "" + sothunhat + "" + sothuba;
    }
}