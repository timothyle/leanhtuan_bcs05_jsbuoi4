/** 
 * Bước 1: nhập input từ user về cạnh thứ nhất, cạnh thứ hai và cạnh thứ ba;
 * Bước 2: nhập các điều kiện như nếu 3 cạnh bằng nhau thì tam giác đều, 2 cạnh bằng nhau thì tam giác cân, cạnh huyền bình phương bằng tổng bình phương 2 cạnh còn lại
 * Bước 3: in kết quả
*/


function xepLoaiTamGiac() {
    const canhthunhat = document.getElementById("canh-thu-nhat").value * 1;
    const canhthuhai = document.getElementById("canh-thu-hai").value * 1;
    const canhthuba = document.getElementById("canh-thu-ba").value * 1;

    if (canhthunhat == canhthuhai && canhthuhai == canhthuba) {
        document.getElementById("result").innerHTML = "Đây là tam giác đều";
    } else if ((canhthunhat == canhthuhai && canhthuba != canhthunhat) || (canhthuhai == canhthuba && canhthunhat != canhthuhai) || (canhthunhat == canhthuba && canhthuhai != canhthunhat)) {
        document.getElementById("result").innerHTML = "Đây là tam giác cân";
    } else if (canhthuba == Math.sqrt((canhthuhai*canhthuhai)+(canhthunhat*canhthunhat)) || canhthuhai == Math.sqrt((canhthunhat*canhthunhat)+(canhthuba*canhthuba))|| canhthunhat == Math.sqrt((canhthuhai*canhthuhai)+(canhthuba*canhthuba))) {
        document.getElementById("result").innerHTML = "Đây là tam giác vuông";
    } else if ((canhthuba == Math.sqrt((canhthuhai*canhthuhai)+(canhthunhat*canhthunhat)) && canhthuhai == canhthunhat)|| (canhthuhai == Math.sqrt((canhthunhat*canhthunhat)+(canhthuba*canhthuba)) && canhthuba == canhthunhat)|| (canhthunhat == Math.sqrt((canhthuhai*canhthuhai)+(canhthuba*canhthuba))&& canhthuba == canhthunhat)) {
            document.getElementById("result").innerHTML = "Đây là tam giác vuông cân";
    } else {
        document.getElementById("result").innerHTML = "Đây là tam giác thường";
    }
}